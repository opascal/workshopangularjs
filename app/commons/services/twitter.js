(function() {
    'use strict';

    var injectParams = ['$http'];
    var TwitterService = function($http) {

        var TwitterService = {};

        TwitterService.getTweets = function(pCallback) {
            $http({
                url: 'http://jsonstub.com/tweets',
                method: 'GET',
                dataType: 'json',
                data: '',
                headers: {
                    'Content-Type': 'application/json',
                    'JsonStub-User-Key': '71e73ed5-38a0-4ed8-a94d-3de37c851142',
                    'JsonStub-Project-Key': 'a9cd3335-4582-4460-ba30-2a6b0d45ce37'
                }
            }).then(
                function successCallback(response) {
                    pCallback(response.data);
                },
                function errorCallback(errResponse) {
                    console.warn(errResponse);
                }
            );
        };

        return TwitterService;
    };

    TwitterService.$inject = injectParams;
    angular.module('workshop').factory('TwitterService', TwitterService);
}());
