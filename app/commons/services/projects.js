(function() {
    'use strict';

    var injectParams = ['$http'];
    var ProjectsService = function($http) {

        var ProjectsService = {};

        ProjectsService.getProjects = function(pCallback) {
            $http({
                url: 'app/commons/json/projects.json'
            }).then(
                function successCallback(response) {
                    pCallback(response.data);
                },
                function errorCallback(errResponse) {
                    console.warn(errResponse);
                }
            );
        };

        ProjectsService.getProject = function(pId, pCallback) {
            $http({
                url: 'app/commons/json/projects.json'
            }).then(
                function successCallback(response) {
                    var item = response.data.filter(function(e) {
                        return e._id === pId;
                    });

                    if (item.length > 0) {
                        pCallback(item[0]);
                    }

                },
                function errorCallback(errResponse) {
                    console.warn(errResponse);
                }
            );
        };

        return ProjectsService;
    };

    ProjectsService.$inject = injectParams;
    angular.module('workshop').factory('ProjectsService', ProjectsService);
}());
