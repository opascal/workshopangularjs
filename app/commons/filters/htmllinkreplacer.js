(function() {
    'use strict';

    var injectParams = ['$sce'];
    var htmllinkreplacer = function($sce) {

        return function(item) {
            // Split each words
            var splitted = item.split(' ');
            // RegExp that matchs a link
            var regLink = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;

            for (var i = 0; i < splitted.length; i++) {
                splitted[i] = splitted[i].replace(regLink, "<a href='"+ splitted[i] +"' target='_blank'>"+ splitted[i] +"</a>");
            }

            return $sce.trustAsHtml(splitted.join(' '));
        };
    };

    htmllinkreplacer.$inject = injectParams;
    angular
        .module('workshop')
        .filter('htmllinkreplacer', htmllinkreplacer);

}());
