(function() {
    'use strict';

    var homepageProject = function() {

        var homepageProject = {
            restrict: 'E',
            replace: true,
            scope: {
                project : '='
            },
            bindToController: true,
            controller: HomepageProjectController,
            controllerAs: 'homepageProjectCtrl',
            templateUrl: 'app/commons/directives/homepageProject/homepageProject.tpl.html'
        };

        return homepageProject;
    };

    angular.module('workshop').directive('homepageProject', homepageProject);

    /**
    * Directive controller
    */

    // HomepageProjectController.inject = [];
    function HomepageProjectController() {
        var vm = this;
    }

}());
