(function() {
    'use strict';

    var injectParams = [];
    var portfolioProvider = function() {

        var portfolio = {
            firstname: 'Prénom',
            lastname: 'Nom',
            avatar: 'http://placehold.it/300x300',
            welcomeTitle: 'Welcome title',
            welcomeMessage: 'Welcome message',
            github: 'http://github.com',
            twitter: 'http://twitter.com',
            instagram: 'http://instagram.com'
        };

        var portfolioProvider = {};

        portfolioProvider.$get = function() {
            return portfolio;
        };

        portfolioProvider.setNode = function(node, value) {
            if(portfolio.hasOwnProperty(node)) {
                portfolio[node] = value;
            } else {
                console.warn('The node '+ node +' doesn\'t exist.');
            }
        };

        portfolioProvider.setNodes = function(obj) {
            for (var variable in obj) {
                portfolioProvider.setNode(variable, obj[variable]);
            }
        };

        return portfolioProvider;
    };

    portfolioProvider.$inject = injectParams;
    angular.module('workshop').provider('$portfolio', portfolioProvider);
}());
