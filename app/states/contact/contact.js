(function() {
    'use strict';


    var ContactRtr = ['$stateProvider', function($stateProvider) {

        $stateProvider.state('contact', {
            url: '/contact',
            controller: 'ContactController as contactCtrl',
            templateUrl: 'app/states/contact/contact.html'
        });

    }];

    angular.module('workshop').config(ContactRtr);

    var injectParams = ['ProjectsService', '$state'];
    var ContactController = function(ProjectsService, $state) {
        var vm = this;

        vm.formData = {};

        vm.formSubmitted = localStorage.getItem('form_submitted') || false;

        ProjectsService.getProjects(function(projects) {
            vm.projects = projects;
        });

        vm.onSubmit = function() {
            // Store submission
            localStorage.setItem('form_submitted', true);

            // Redirection
            $state.go('home');
        };

        vm.deleteLocalStorage = function() {
            // Clear local storage
            localStorage.clear();

            // Reload contact state
            $state.reload();
        };
    };

    ContactController.$inject = injectParams;
    angular.module('workshop').controller('ContactController', ContactController);

}());
