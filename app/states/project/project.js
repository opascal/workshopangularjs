(function() {
    'use strict';


    var ProjectRtr = ['$stateProvider', function($stateProvider) {

        $stateProvider.state('project', {
            url: '/project/:id',
            controller: 'ProjectController as projectCtrl',
            templateUrl: 'app/states/project/project.html'
        });

    }];

    angular.module('workshop').config(ProjectRtr);

    var injectParams = ['$stateParams', 'ProjectsService'];
    var ProjectController = function($stateParams, ProjectsService) {
        var vm = this;

        ProjectsService.getProject($stateParams.id, function(project) {
            vm.project = project;
        });
    };

    ProjectController.$inject = injectParams;
    angular.module('workshop').controller('ProjectController', ProjectController);

}());
