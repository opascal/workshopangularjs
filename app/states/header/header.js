(function() {
    'use strict';

    var injectParams = ['$portfolio'];
    var HeaderController = function($portfolio) {
        var vm = this;

        vm.texts = $portfolio;
    };

    HeaderController.$inject = injectParams;
    angular.module('workshop').controller('HeaderController', HeaderController);

}());
