(function() {
    'use strict';


    var HomeRtr = ['$stateProvider', function($stateProvider) {

        $stateProvider.state('home', {
            url: '/home',
            controller: 'HomeCtrl as homeCtrl',
            templateUrl: 'app/states/home/home.html'
        });

    }];

    angular.module('workshop').config(HomeRtr);

    var injectParams = ['$portfolio', 'ProjectsService'];
    var HomeCtrl = function($portfolio, ProjectsService) {
        var vm = this;

        vm.texts = $portfolio;

        ProjectsService.getProjects(function(projects) {
            vm.projects = projects;
        });
    };

    HomeCtrl.$inject = injectParams;
    angular.module('workshop').controller('HomeCtrl', HomeCtrl);

}());
