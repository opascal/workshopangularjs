(function() {
    'use strict';

    var injectParams = ['TwitterService', '$portfolio'];
    var FooterController = function(TwitterService, $portfolio) {
        var vm = this;

        TwitterService.getTweets(function(tweets) {
            vm.tweets = tweets;
        });

        vm.texts = $portfolio;
    };

    FooterController.$inject = injectParams;
    angular.module('workshop').controller('FooterController', FooterController);

}());
