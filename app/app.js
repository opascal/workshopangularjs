(function() {
    'use strict';

    angular
    .module('workshop', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', '$portfolioProvider', function($stateProvider, $urlRouterProvider, $portfolioProvider) {
        $urlRouterProvider.otherwise("/home");

        $portfolioProvider.setNodes({
            firstname: 'Olivier',
            lastname: 'PASCAL',
            firstname: 'Olivier',
            lastname: 'PASCAL',
            avatar: 'https://pbs.twimg.com/profile_images/460200335219240962/9YpzdBIA_400x400.png',
            welcomeTitle: 'Développeur Front-end chez Extia',
            welcomeMessage: 'Bienvenue sur mon portfolio, découvrez mes dernières réalisations et n\'hésitez pas à me contacter.',
            github: 'https://github.com/pascaloliv',
            twitter: 'https://twitter.com/pascaloliv',
            instagram: 'https://instagram.com/pascaloliv'
        });
    }]);

})();
